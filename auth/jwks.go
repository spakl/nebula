package auth

import (
	"context"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"os"
	"strings"

	"github.com/golang-jwt/jwt/v5"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"

	"time"

	"gitlab.com/spakl/nebula/v2/logger"
	"gitlab.com/spakl/nebula/v2/otel"
	"gitlab.com/spakl/nebula/v2/utils"
)

// JWK represents a JSON Web Key.
type JWK struct {
    Keys []struct {
        Kid string `json:"kid"`
        Kty string `json:"kty"`
        Use string `json:"use"`
        Alg string `json:"alg"`
        N   string `json:"n"`
        E   string `json:"e"`
    } `json:"keys"`
}

var JWKS JWK = JWK{}

func (a *OAuthClient) FetchJWKS(ctx context.Context) (error) {
     // Fetch JWKS from the .well-known URL
     req, err := http.NewRequestWithContext(ctx, "GET", a.JwksURL, nil)
     if err != nil {
         return fmt.Errorf("failed to create JWKS request: %w", err)
     }
 
     client := &http.Client{}
 
     resp, err := client.Do(req)
     if err != nil {
         return fmt.Errorf("failed to fetch JWKS: %w", err)
     }
     defer resp.Body.Close()
 
     var jwks JWK
     if err := json.NewDecoder(resp.Body).Decode(&jwks); err != nil {
         return fmt.Errorf("failed to decode JWKS: %w", err)
     }

     JWKS = jwks

     return nil
}

// StartJWKSRefreshWorker starts a worker that periodically refreshes the JWKS.
func (a *OAuthClient) StartJWKSRefreshWorker() {
	refreshEnabled := viper.GetBool("auth.jwksRefresh")

    if !refreshEnabled {
        return
    }

    ctx := context.Background()

	log := logger.GetTraceLogger(ctx)

	// Set default refresh rate to 24 hours if not configured.
	refreshRateStr := viper.GetString("auth.jwksRefreshRate")
	if refreshRateStr == "" {
		refreshRateStr = "24h"
	}

	// Parse the refresh rate from string to time.Duration.
	refreshRate, err := time.ParseDuration(refreshRateStr)
	if err != nil {
		log.Errorf("invalid jwksRefreshRate value: %v", err)
        ctx.Done()
	}

	ticker := time.NewTicker(refreshRate)
	defer ticker.Stop()

	for {
		select {
		case <-ticker.C:
			log.Println("Refreshing JWKS...")
			if err := a.FetchJWKS(ctx); err != nil {
				log.Printf("Failed to refresh JWKS: %v", err)
			}
		case <-ctx.Done():
			log.Println("JWKS refresh worker stopped.")
			return
		}
	}
}

func (jwk *JWK)  ParseToken(token string) (*jwt.Token, error){
        // Parse the JWT token
        parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
        if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
            return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
        }

        kid, ok := token.Header["kid"].(string)
        if !ok {
            return nil, errors.New("kid not present in token header")
        }

        // Find the key with the matching KID
        var rsaPublicKey *rsa.PublicKey
        var err error
        for _, key := range jwk.Keys {
            if key.Kid == kid {
                rsaPublicKey, err = convertToRSAPublicKey(key.N, key.E)
                if err != nil {
                    return nil, fmt.Errorf("failed to convert key: %w", err)
                }
                break
            }
        }

        if rsaPublicKey == nil {
            return nil, errors.New("no matching key found")
        }

        return rsaPublicKey, nil
    })

    if err != nil {
        return nil, fmt.Errorf("failed to parse token: %w", err)
    }
    return parsedToken, nil
}

// ValidateToken validates the JWT token using the JWKS from the well-known URL.
func (a *OAuthClient) ValidateTokenJWKS(ctx context.Context, token string) (bool, error) {
    // Parse the JWT token
    parsedToken, err := JWKS.ParseToken(token)
    if err != nil {
        return false, err
    }
    return parsedToken.Valid, nil
}

// RevokeToken revokes the given token.
func (c *OAuthClient) RevokeToken(ctx context.Context, token string) error {
	ctx, span := otel.CreateSpan(ctx, span_prefix + ".RevokeToken")
	defer span.End()
    
    req, err := http.NewRequest("POST", c.RevokeURL, strings.NewReader("token="+token))
	if err != nil {
		return err
	}
	req.SetBasicAuth(c.ClientID, c.ClientSecret)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
    otel.InjectTraceContext(ctx, req)
    
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to revoke token: %s", resp.Status)
	}

	return nil
}


// ValidateToken validates the JWT token using the JWKS from the well-known URL.
func (a *OAuthClient) TokenRoles(token string) ([]string, error) {
    // Parse the JWT token
    parsedToken, err := JWKS.ParseToken(token)
    if err != nil {
        return nil, err
    }

    claims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok {
		return nil, fmt.Errorf("Failed to extract claims from jwt")
	}

    if groups, ok := claims[a.RolesClaimKey].([]interface{}); ok {
        var result []string 
        for _, group := range groups{
            if groupStr, ok := group.(string); ok {
                result = append(result, groupStr)
            }
        }
        return result, nil
    }

    return nil, nil
}


// convertToRSAPublicKey converts the modulus (n) and exponent (e) into an RSA public key.
func convertToRSAPublicKey(nStr, eStr string) (*rsa.PublicKey, error) {
    // Decode the base64 URL-encoded modulus (n)
    nBytes, err := base64.RawURLEncoding.DecodeString(nStr)
    if err != nil {
        return nil, err
    }

    // Decode the base64 URL-encoded exponent (e)
    eBytes, err := base64.RawURLEncoding.DecodeString(eStr)
    if err != nil {
        return nil, err
    }

    // Convert exponent bytes to an integer
    eInt := new(big.Int).SetBytes(eBytes).Int64()

    // Construct an rsa.PublicKey
    pubKey := &rsa.PublicKey{
        N: new(big.Int).SetBytes(nBytes), // Set the modulus
        E: int(eInt),                     // Set the exponent
    }

    return pubKey, nil
}


// SaveTokenToFile saves the OAuth token to the specified file, replacing the file if it exists.
func (c *OAuthClient) SaveTokenToFile(ctx context.Context, token *oauth2.Token, filePath string) error {
	_, span := otel.CreateSpan(ctx, span_prefix + ".SaveTokenToFile")
	defer span.End()

	log := logger.GetTraceLogger(ctx)

	// Marshal the token to JSON
	bytes, err := json.MarshalIndent(token, "", "  ")
	if err != nil {
		log.Error("Failed to marshal token: ", err)
		return fmt.Errorf("failed to marshal token: %w", err)
	}

	// Write the JSON to the file
	err = os.WriteFile(filePath, bytes, 0644)
	if err != nil {
		log.Error("Failed to write token to file: ", err)
		return fmt.Errorf("failed to write token to file: %w", err)
	}

	// Log a success message
	log.Info("Token saved successfully to:", filePath)
	return nil
}

// GetTokenFromFile reads the token from the specified file.
func (c *OAuthClient) GetTokenFromFile(ctx context.Context, filePath string) (*oauth2.Token, error) {
	_, span := otel.CreateSpan(ctx, span_prefix + ".GetTokenFromFile")
	defer span.End()

    file, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("failed to open token file: %w", err)
	}
	defer file.Close()

	var token oauth2.Token
	if err := json.NewDecoder(file).Decode(&token); err != nil {
		return nil, fmt.Errorf("failed to decode token from file: %w", err)
	}

	return &token, nil
}

func GetTokenFile(global bool, tokenFileName string) string {
	var filepath string

	if global {
		globalDir, err := utils.GetGlobalDir()
		if err != nil {
			logrus.Fatal(err.Error())
		}
		filepath = globalDir + "/tokens/" + tokenFileName
	} else {
		filepath = ".spakl/tokens/"  + tokenFileName
	}
	return filepath
}
