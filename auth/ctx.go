package auth

import "context"

// Register condition handlers dynamically
type AuthToken string

const (
	TokenKey AuthToken = "AuthContext"
)

type AuthContext struct {
    Token       string            // IP address of the requester
    User      	UserInfo         // Current time
	Roles 		[]string
}

func SetAuthContext(ctx context.Context, token AuthContext) context.Context {
	return context.WithValue(ctx,TokenKey, token)
}

func GetAuthContext(ctx context.Context) AuthContext {
	return ctx.Value(TokenKey).(AuthContext)
}

func (ctx *AuthContext) HasRole(role string) bool {
	for _, r := range ctx.Roles {
		if r == role {
			return true
		}
	}
	return false
}

func (a *OAuthClient) NewAuthContext(ctx context.Context, token string) (AuthContext, error) {
	
	user, err := TokenUserInfo(token)
	if err != nil {
		return AuthContext{}, err
	}

	roles, err := a.TokenRoles(token)
	if err != nil {
		return AuthContext{}, err
	}

	return AuthContext{
		Token: token,
		User:  user,
		Roles: roles,
	}, nil
}