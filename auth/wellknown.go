package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/spakl/nebula/v2/otel"
)

// fetchWellKnownConfig fetches OAuth URLs from the well-known configuration endpoint.
func (c *OAuthClient) FetchWellKnownConfig(ctx context.Context,wellKnownURL string) error {
	_, span := otel.CreateSpan(ctx, span_prefix + ".FetchWellKnownConfig")
	defer span.End()


    resp, err := http.Get(wellKnownURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to fetch well-known config: %s", resp.Status)
	}

	var config struct {
		AuthURL     string `json:"authorization_endpoint"`
		TokenURL    string `json:"token_endpoint"`
		UserInfoURL string `json:"userinfo_endpoint"`
		RevokeURL   string `json:"revocation_endpoint"`
		JwksURL     string `json:"jwks_uri"`
	}

	if err := json.NewDecoder(resp.Body).Decode(&config); err != nil {
		return err
	}

	c.AuthURL = config.AuthURL
	c.TokenURL = config.TokenURL
	c.UserInfoURL = config.UserInfoURL
	c.RevokeURL = config.RevokeURL
	c.JwksURL = config.JwksURL

	return nil
}
