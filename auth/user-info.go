package auth

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/spakl/nebula/v2/logger"
	"gitlab.com/spakl/nebula/v2/otel"
	"golang.org/x/oauth2"
)

// RoleMapping represents the mapping of roles to their identifiers
type RoleMapping map[string]map[string]string

// UserInfo represents the structure of the user info JSON
type UserInfo struct {
	Email                string       `json:"email"`
	EmailVerified        bool         `json:"email_verified"`
	FamilyName           string       `json:"family_name"`
	Gender               string       `json:"gender"`
	GivenName            string       `json:"given_name"`
	Groups               []string     `json:"groups"`
	Roles                []string     `json:"roles"`
	Locale               string       `json:"locale"`
	Name                 string       `json:"name"`
	Nickname             string       `json:"nickname"`
	PreferredUsername    string       `json:"preferred_username"`
	Sub                  string       `json:"sub"`
	UpdatedAt            int64        `json:"updated_at"`
	OrganizationRoles    RoleMapping  `json:"urn:zitadel:iam:org:project:roles"`
}

// DecodeUserInfo decodes the base64 encoded JSON string into a UserInfo struct
func DecodeUserInfo(encoded string) (*UserInfo, error) {
	fmt.Println(encoded)
	// Decode the base64 string
	decoded, err := base64.StdEncoding.DecodeString(encoded)
	if err != nil {
		return nil, fmt.Errorf("failed to decode base64 string: %w", err)
	}

	// Unmarshal the JSON into the UserInfo struct
	var userInfo UserInfo
	if err := json.Unmarshal(decoded, &userInfo); err != nil {
		return nil, fmt.Errorf("failed to unmarshal JSON: %w", err)
	}

	return &userInfo, nil
}


func TokenUserInfo(token string) (UserInfo, error) {
	var userInfo UserInfo

	parsedToken, err := JWKS.ParseToken(token)
	if err != nil {
		return userInfo, err
	}

	claims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok {
		return userInfo, fmt.Errorf("Failed to extract claims from jwt")
	}

	jsonBytes, err := json.Marshal(claims)
	if err != nil {
		return userInfo, err
	}

	if err := json.Unmarshal(jsonBytes, &userInfo); err != nil {
		return userInfo, err
	}

	return userInfo, nil
}


// ComputeCodeChallenge generates the PKCE code challenge.
func ComputeCodeChallenge(codeVerifier string) string {
	sha256Hash := sha256.Sum256([]byte(codeVerifier))
	return base64.URLEncoding.WithPadding(base64.NoPadding).EncodeToString(sha256Hash[:])
}

// Login handles the OAuth login flow with PKCE.
func (c *OAuthClient) Login(ctx context.Context, code string, codeVerifier string) (*oauth2.Token, error) {
	ctx, span := otel.CreateSpan(ctx, span_prefix + "..Login")
	defer span.End()
    
    oauth2Config := c.Config
	oauth2Config.Endpoint.AuthStyle = oauth2.AuthStyleInHeader

	authCodeOptions := []oauth2.AuthCodeOption{
		oauth2.SetAuthURLParam("code_verifier", codeVerifier),
		oauth2.AccessTypeOnline,
		oauth2.AccessTypeOffline,
	}
	token, err := oauth2Config.Exchange(ctx, code, authCodeOptions...)
	if err != nil {
		return nil, err
	}
	return token, err
}

// Logout handles the OAuth logout flow.
func (c *OAuthClient) Logout(ctx context.Context, token string) error {
    _, span := otel.CreateSpan(ctx, span_prefix + ".Logout")
	defer span.End()
	// Implement logout logic if applicable.
	return nil
}


// GetGroupsFromToken extracts groups from the given token.
func (c *OAuthClient) GetUserInfo(ctx context.Context, token string) (*UserInfo, error) {
	ctx, span := otel.CreateSpan(ctx, span_prefix + ".GetRolesFromAuth")
	defer span.End()
	log := logger.GetTraceLogger(ctx)


	// Create a new HTTP request
	req, err := http.NewRequest("GET", c.UserInfoURL, nil)
	if err != nil {
		log.Errorf("Error creating request: %v", err)
		return nil, err
	}

	// Add the Authorization header
	req.Header.Add("Authorization", "Bearer " + token)

	// Create an HTTP client and execute the request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Errorf("Error making request: %v", err)
		return nil, err
	}
	defer resp.Body.Close()

	// Check if the request was successful
	if resp.StatusCode != http.StatusOK {
		log.Errorf("Request failed with status: %s", resp.Status)
		return nil, err
	}

	// Read the response body
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Errorf("Error reading response body: %v", err)
		return nil, err
	}


	var info *UserInfo
	if err := json.Unmarshal(body, &info); err != nil {
		return &UserInfo{}, err
	}

	return info, err
}

