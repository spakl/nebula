package auth

import (
	"context"
	"fmt"

	"github.com/spf13/viper"
	"golang.org/x/oauth2"

	"gitlab.com/spakl/nebula/v2/otel"
)
const span_prefix = "nebula.auth"

type OAuthClient struct {
	RolesClaimKey string
	ClientID     string
	ClientSecret string
	RedirectURI  string
	AuthURL      string
	TokenURL     string
	UserInfoURL  string
	RevokeURL    string
	JwksURL      string
	Config       *oauth2.Config
	UsePKCE      bool
}

var OauthClient OAuthClient


// NewOAuthClient initializes a new OAuth client.
func NewOAuthClient(ctx context.Context,rolesClaimKey, clientID, clientSecret, redirectURI, wellKnownURL string, usePKCE bool) (*OAuthClient, error) {
	ctx, span := otel.CreateSpan(ctx, span_prefix + ".NewOAuthClient")
	defer span.End()

    client := &OAuthClient{
		RolesClaimKey: rolesClaimKey,
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURI:  redirectURI,
		UsePKCE:      usePKCE,
	}

	if err := client.FetchWellKnownConfig(ctx, wellKnownURL); err != nil {
		return nil, err
	}

	client.Config = &oauth2.Config{
		ClientID: clientID,
		RedirectURL:  redirectURI,
		Endpoint: oauth2.Endpoint{
			AuthURL:  client.AuthURL,
			TokenURL: client.TokenURL,
		},
	}

	if !usePKCE {
		client.Config.ClientSecret = clientSecret
	}

	err := client.FetchJWKS(ctx)
	if err != nil {
		return nil, err
	}

	go client.StartJWKSRefreshWorker()

	OauthClient = *client

	return client, nil
}



func InitAuthClient(ctx context.Context) (*OAuthClient, error){
	ctx, span := otel.CreateSpan(ctx, span_prefix + ".InitAuthClient")
	defer span.End()

	// logg := logger.GetTraceLogger(ctx)

	rolesClaimKey := viper.GetString("auth.rolesClaimKey")
	clientID := viper.GetString("auth.clientID")
	clientSecret := viper.GetString("auth.clientSecret")
	redirectURI := viper.GetString("auth.redirectURI")
	wellKnownURL := viper.GetString("auth.wellKnownURL")
	callbackPort := viper.GetString("auth.callbackPort")
	usePKCE := viper.GetBool("auth.pkce")
	scopes := viper.GetStringSlice("auth.scopes")

	if callbackPort == "" {
		callbackPort = "32800"
	}

	missingParams := []string{}
	if clientID == "" {
		missingParams = append(missingParams, "Missing clientID, set in auth.clientID")
	}
	if wellKnownURL == "" {
		missingParams = append(missingParams, "Missing wellKnownURL, set in auth.wellKnownURL")
	}
	if len(scopes) == 0 {
		missingParams = append(missingParams, "Missing Scopes, set in auth.scopes")
	}
	
	if len(missingParams) > 0 {
		return nil, fmt.Errorf("Missing required authentication parameters: %v", missingParams)
	}

	return NewOAuthClient(
		ctx,
		rolesClaimKey,
		clientID,
		clientSecret,
		redirectURI,
		wellKnownURL,
		usePKCE,
	)

}