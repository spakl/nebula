package auth

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/spakl/nebula/v2/otel"
	"gitlab.com/spakl/nebula/v2/utils"
)

// PolicyMiddleware is the middleware that validates the policy
func (a *OAuthClient)  AuthenticationMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx, span := otel.CreateSpan(r.Context(), "AuthenticationMiddleware")
			defer span.End()
			
			token := r.Header.Get("Authorization")

			if strings.Contains(token, "Bearer ") {
				token = strings.TrimPrefix(token, "Bearer ")
			}
			if token == "" {
				utils.SendError(ctx,r,w, fmt.Errorf("token is missing, include in Authorization header"), http.StatusBadRequest)
				return
			}

			valid, err := a.ValidateTokenJWKS(ctx, token)
			if err != nil {
				utils.SendError(ctx,r,w, err, http.StatusForbidden)
				return
			}
			if !valid {
				utils.SendError(ctx, r, w, fmt.Errorf("access denied"), http.StatusForbidden)
				return

			}

			authCtx, err := a.NewAuthContext(ctx, token)
			if err != nil {
				utils.SendError(ctx,r,w, err, http.StatusForbidden)
				return
			}

			ctx = SetAuthContext(ctx, authCtx)
			
			// Continue to the next handler if the policy allows the action
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}