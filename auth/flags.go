package auth

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func InitFlags(cmd *cobra.Command) {

	// Define persistent flags
	cmd.PersistentFlags().String("auth-rolesClaimKey", "roles", "The claim key for roles")
	cmd.PersistentFlags().String("auth-jwksRefreshRate", "6h", "JWKS Refresh Rate")
	cmd.PersistentFlags().Bool("auth-jwksRefresh", true, "Refresh JWKs")
	cmd.PersistentFlags().String("auth-clientID", "", "The client ID for authentication")
	cmd.PersistentFlags().String("auth-clientSecret", "", "The client secret for authentication")
	cmd.PersistentFlags().String("auth-redirectURI", "", "The redirect URI for OAuth flow")
	cmd.PersistentFlags().String("auth-wellKnownURL", "https://auth.spakl.io/.well-known/openid-configuration", "The well-known URL for OIDC")
	cmd.PersistentFlags().String("auth-callbackPort", "30800", "The callback port for OAuth")
	cmd.PersistentFlags().Bool("auth-usePKCE", true, "Use PKCE for OAuth flow")
	cmd.PersistentFlags().StringSlice("auth-scopes", []string{"openid","user","profile", "email"}, "Scopes for the OAuth flow")

	// Bind flags with Viper
	viper.BindPFlag("auth.rolesClaimKey", cmd.PersistentFlags().Lookup("auth-rolesClaimKey"))
	viper.BindPFlag("auth.clientID", cmd.PersistentFlags().Lookup("auth-clientID"))
	viper.BindPFlag("auth.clientSecret", cmd.PersistentFlags().Lookup("auth-clientSecret"))
	viper.BindPFlag("auth.redirectURI", cmd.PersistentFlags().Lookup("auth-redirectURI"))
	viper.BindPFlag("auth.wellKnownURL", cmd.PersistentFlags().Lookup("auth-wellKnownURL"))
	viper.BindPFlag("auth.callbackPort", cmd.PersistentFlags().Lookup("auth-callbackPort"))
	viper.BindPFlag("auth.pkce", cmd.PersistentFlags().Lookup("auth-usePKCE"))
	viper.BindPFlag("auth.scopes", cmd.PersistentFlags().Lookup("auth-scopes"))
	viper.BindPFlag("auth.jwksRefreshRate", cmd.PersistentFlags().Lookup("auth-jwksRefreshRate"))
	viper.BindPFlag("auth.jwksRefresh", cmd.PersistentFlags().Lookup("auth-jwksRefresh"))
}