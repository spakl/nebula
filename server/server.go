package api

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/spakl/nebula/v2/logger"
	"gitlab.com/spakl/nebula/v2/otel"
	"gitlab.com/spakl/nebula/v2/utils"

	"github.com/go-chi/chi/v5"
)


type Server struct {
	Router chi.Router
	Addr string
}

func NewApi(ctx context.Context, addr string, router chi.Router) *Server {
	server := &Server{
		Router: router,
		Addr: addr,
	}

	router.Get("/health", server.Health)

	return server 
}



// Serve starts the HTTP server in a goroutine and sends errors to the provided error channel.
func (api *Server) Serve(ctx context.Context, errChan chan error, useTls bool) {
	ctx, span := otel.CreateSpan(ctx, "api.Serve")
	defer span.End()

	log := logger.GetTraceLogger(ctx)

	var server *http.Server
	if useTls {
		log.Info("serving with tls")
		tlsConfig := utils.ConfigureTls()
		server = &http.Server{
			Addr:         api.Addr,
			Handler:      api.Router,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
			IdleTimeout:  15 * time.Second,
			TLSConfig: tlsConfig,
		}
	} else {
		log.Info("serving with without tls")

		server = &http.Server{
			Addr:         api.Addr,
			Handler:      api.Router,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
			IdleTimeout:  15 * time.Second,
		}
	}

	go func() {
		log.Infof("Starting server on %s", api.Addr)
		if useTls {
			if err := server.ListenAndServeTLS("",""); err != nil && err != http.ErrServerClosed {
				log.Errorf("Server error: %v", err)
				errChan <- err // Send the error to the error channel
			}
		} else {
			if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				log.Errorf("Server error: %v", err)
				errChan <- err // Send the error to the error channel
			}
		}
	}()

	// Listen for context cancellation or errors
	go func() {
		<-ctx.Done() // Wait for shutdown signal
		log.Info("Shutting down server...")

		// Gracefully shut down the server
		ctxShutDown, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		log = logger.GetTraceLogger(ctxShutDown)
		log.Info("Shutting down server...")

		otel.Flush(ctxShutDown)

		if err := server.Shutdown(ctxShutDown); err != nil {
			log.Errorf("Server forced to shutdown: %v", err)
			errChan <- err
		}
	}()
}


func (api *Server) Health(w http.ResponseWriter, r *http.Request) {
	// _, span := otel.CreateSpan(r.Context(), "api.Health")
	// defer span.End()

	// Return success message
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"status": "healthy"})
}