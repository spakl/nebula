package api

import (
	"context"
	"gitlab.com/spakl/nebula/v2/otel"
	"runtime"
	"time"

	"github.com/shirou/gopsutil/v4/cpu"
	"github.com/shirou/gopsutil/v4/disk"
	"github.com/shirou/gopsutil/v4/net"
	"go.opentelemetry.io/otel/metric"
)

const metricTickerInterval = 5 * time.Second
var Mb uint64 = 1_048_576 // number of bytes in a MB
var MetricTicker = time.NewTicker(metricTickerInterval)

func CollectRuntimeResourceMetrics() {
	
    for {
		select {
		case <-MetricTicker.C:
            // This will be executed every "period" of time passes
			otel.NewObsGuageFloat("dfs.api.forms.process.allocated_memory","Allocated Memory in Mb","Mb",func(_ context.Context, fo metric.Float64Observer) error{
				var memStats runtime.MemStats
				runtime.ReadMemStats(&memStats)
		
				allocatedMemoryInMB := float64(memStats.Alloc) / float64(Mb)
				fo.Observe(allocatedMemoryInMB)
		
				return nil
			})

			// This will be executed every "period" of time passes
			otel.NewObsGuageFloat("dfs.api.forms.process.cpu_usage","the number of logical CPUs usable by the current process.","cores",func(_ context.Context, fo metric.Float64Observer) error{
				cpuUsage := runtime.NumCPU()
				runtime.NumGoroutine()
				fo.Observe(float64(cpuUsage))
				return nil
			})

			// This will be executed every "period" of time passes
			otel.NewObsGuageFloat("dfs.api.forms.process.go_routines","the number of goroutines that currently exist","routines",func(_ context.Context, fo metric.Float64Observer) error{
				routineUsage := runtime.NumGoroutine()
				
				fo.Observe(float64(routineUsage))
				return nil
			})

			// This will be executed every "period" of time passes
			otel.NewObsGuageFloat("dfs.api.forms.process.cgo_calls"," the number of cgo calls made by the current process","calls",func(_ context.Context, fo metric.Float64Observer) error{
				routineUsage := runtime.NumCgoCall()
				
				fo.Observe(float64(routineUsage))
				return nil
			})

        }
    }
}

func CollectDiskIOMetrics() {
		for range MetricTicker.C {
			d, _ := disk.IOCounters()
			
			for _, stat := range d {
				metric_options := otel.WithMetricAttributes(
					otel.NewStringAttr("name", stat.Name),
					otel.NewStringAttr("label", stat.Label),
					
				)

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.IopsInProgress", "Number of Iops in progress", "operations", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.IopsInProgress), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.WeightedIO", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.WeightedIO), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.IoTime", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.IoTime), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.ReadCount", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.ReadCount), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.WriteCount", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.WriteCount), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.WriteCount", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.ReadBytes), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.ReadBytes", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.ReadBytes), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.WriteBytes", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.WriteBytes), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.MergedWriteCount", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.MergedWriteCount ), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.disk.MergedReadCount", "", "", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.MergedReadCount  ), metric_options)
					return nil
				})

			}
		}
}

func CollectNetIOMetrics() {
		for range MetricTicker.C {
			d, _ := net.IOCounters(false)
			for _, stat := range d {
				metric_options := otel.WithMetricAttributes(
					otel.NewStringAttr("name", stat.Name),				
				)

				otel.NewObsGuageFloat("dfs.api.forms.process.net.BytesRecv", "number of bytes received", "byte", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.BytesRecv), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.net.BytesSent", "number of bytes sent", "byte", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.BytesSent), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.net.Errin", "total number of errors while receiving", "error", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.Errin), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.net.Errout", "total number of errors while sending", "error", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.Errout), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.net.PacketsSent", "number of packets sent", "packet", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.PacketsSent), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.net.PacketsRecv", "number of packets received", "packet", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.PacketsRecv), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.net.Dropin", "total number of incoming packets which were dropped", "packet", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.Dropin), metric_options)
					return nil
				})

				otel.NewObsGuageFloat("dfs.api.forms.process.net.Dropout", "total number of outgoing packets which were dropped (always 0 on OSX and BSD)", "packet", func(_ context.Context, fo metric.Float64Observer) error {
					fo.Observe(float64(stat.Dropout), metric_options)
					return nil
				})
			}
		}
}

func CollectCpuIOMetrics() {
	for range MetricTicker.C {
		d, _ := cpu.Percent(0, false)
		for _, stat := range d {
			metric_options := otel.WithMetricAttributes(
			)

			otel.NewObsGuageFloat("dfs.api.forms.process.cpu.PercentUtilized", "calculates the percentage of cpu used either per CPU or combined", "percent", func(_ context.Context, fo metric.Float64Observer) error {
				fo.Observe(stat, metric_options)
				return nil
			})
		}
	}
}