package otel

import (
	"github.com/sirupsen/logrus"
	"net/http"
	noop "go.opentelemetry.io/otel/trace/noop"
	"go.opentelemetry.io/otel"
	"time"

)

func OtelMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if otel.GetTracerProvider() == noop.NewTracerProvider() {
			next.ServeHTTP(w, r)
			return
		}
		
		start := time.Now()
		
		// Starts span with name of route
		ctx, span := CreateSpan(r.Context(), r.URL.String())
		defer span.End()
		
		apiCounter, err  := NewCounter("api.counter", "Number of API calls.", "{call}")
		if err != nil {
			logrus.Error(err.Error())
		}
		apiCounter.Add(ctx, 1)
		
		requestHistogram, err := NewHistogram("api.reqDuration","The duration of request execution.","s")
		if err != nil {
			logrus.Error(err.Error())
		}
		defer func (startTime time.Time){
			fini := time.Since(startTime).Seconds()
			requestHistogram.Record(ctx, fini)
		}(start)
		
		next.ServeHTTP(w, r.WithContext(ctx))
		
	})
}
