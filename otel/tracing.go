package otel

import (
	"context"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"

	// "go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"go.opentelemetry.io/otel/sdk/resource"
	sdk "go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/trace"
)

var (
	TraceProvider *sdk.TracerProvider
	Tracer trace.Tracer 
)

func NewProvider(res *resource.Resource, traceExporter sdk.SpanExporter) *sdk.TracerProvider{

	batcher := sdk.NewBatchSpanProcessor(traceExporter,
		sdk.WithMaxQueueSize(2048),        // Queue up to 2048 spans
		sdk.WithBatchTimeout(5*time.Second), // Send batch every 5 seconds
		sdk.WithMaxExportBatchSize(512),
	)

	return sdk.NewTracerProvider(
		sdk.WithSampler(sdk.AlwaysSample()),
		sdk.WithResource(res),
		sdk.WithSpanProcessor(batcher),
	)


}

func initTracerProvider(ctx context.Context) (trace.TracerProvider, error) {
	otelEndpoint := viper.GetString("otel.endpoint")

	exporter := viper.GetString("otel.traces.exporter")

	res, err := getResource(ctx)
	if err != nil {
		return nil, err
	}
	
	var traceExporter sdk.SpanExporter
	switch exporter {
	case "otel":
		traceExporter, err = newTracingExporter(ctx, otelEndpoint)
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
	case "stdout":
		traceExporter, err = newStdoutExporter()
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
	default:
		traceExporter, err = newStdoutExporter()
		if err != nil {
			logrus.Error(err)
			return nil, err
		}
	}

	TraceProvider = NewProvider(res, traceExporter)

	prop := newPropagator()
	otel.SetTextMapPropagator(prop)
	otel.SetTracerProvider(TraceProvider)
	
	// Shutdown will flush any remaining spans and shut down the exporter.
	return TraceProvider, nil
}

func newTracingExporter(ctx context.Context, otelEndpoint string) (sdk.SpanExporter, error) {
	return otlptracehttp.New(ctx,
		// otlptracehttp.WithEndpoint(config.Endpoint),
		otlptracehttp.WithEndpointURL(otelEndpoint),
		otlptracehttp.WithTimeout(time.Second*10),
		// otlptracehttp.WithTLSClientConfig(tlsConfig),
		otlptracehttp.WithInsecure(),
	)
}

func newStdoutExporter() (sdk.SpanExporter, error) {
	return stdouttrace.New(stdouttrace.WithPrettyPrint())
}


func CreateSpan(ctx context.Context, spanName string) (context.Context, trace.Span) {
	context, span := Tracer.Start(ctx, spanName)
	return context, span
}

func CreateSpanWithAttr(ctx context.Context, spanName, key, value string) (context.Context, trace.Span) {
	context, span := Tracer.Start(ctx, spanName, trace.WithAttributes(NewStringAttr(key, value)))
	return context, span
}

func EnhanceSpanWithRequest(span trace.Span, r *http.Request) {
	span.SetAttributes(
		NewStringAttr("RequestURI", r.RequestURI),
		NewStringAttr("Method", r.Method),
		NewStringAttr("Proto", r.Proto),
		NewInt64Attr("ContentLength", r.ContentLength),
		NewStringAttr("Host", r.Host),
		NewStringAttr("RemoteAddr", r.RemoteAddr),
	)
}


func GetSpanFromCtx(ctx context.Context) trace.Span {
	return trace.SpanFromContext(ctx)
}

func AddErrorEvent(span trace.Span, err error ) {
	span.AddEvent("Error", trace.WithAttributes(
		NewStringAttr("Error", err.Error())),
	)
	span.SetStatus(codes.Error, err.Error())
	span.End()
}

func AddErrorEventWAttr(span trace.Span, err error, kv ...attribute.KeyValue) {
	kv = append(kv, NewStringAttr("Error", err.Error()))
	span.AddEvent("Error", trace.WithAttributes(
		kv...,
	))
	span.SetStatus(codes.Error, err.Error())
	span.End()
}

