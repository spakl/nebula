package otel

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	sdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.25.0"
	"go.opentelemetry.io/otel/trace/noop"
	moop "go.opentelemetry.io/otel/metric/noop"
)



func getResource(ctx context.Context) (*resource.Resource, error) {
	apiVersion := viper.GetString("version")
	serviceName := viper.GetString("otel.service_name")
	return	resource.New(ctx,
		resource.WithFromEnv(),      // Discover and provide attributes from OTEL_RESOURCE_ATTRIBUTES and OTEL_SERVICE_NAME environment variables.
		resource.WithTelemetrySDK(), // Discover and provide information about the OpenTelemetry SDK used.
		resource.WithProcess(),      // Discover and provide process information.
		resource.WithOS(),           // Discover and provide OS information.
		resource.WithContainer(),    // Discover and provide container information.
		resource.WithContainerID(),  // Discover and provide container information.
		resource.WithHost(),
		resource.WithAttributes(
			semconv.ServiceName(serviceName),
			semconv.ServiceVersion(apiVersion),
		), 
	)
}



// Initialize the Tracer and TracerProvider globally
func InitOtel() {
	ctx := context.Background()
	
	viper.SetDefault("otel.traces.enabled", false)
	viper.SetDefault("otel.metrics.enabled", false)
	viper.SetDefault("otel.logs.enabled", false)
	

	traceEnabled := viper.GetBool("otel.traces.enabled")
	metricsEnabled := viper.GetBool("otel.metrics.enabled")
	logsEnabled := viper.GetBool("otel.logs.enabled")
	serviceName := viper.GetString("otel.service_name")
	
	if traceEnabled {
		provider, err := initTracerProvider(ctx)
		if err != nil {
			logrus.Fatal(err)
		}
		otel.SetTracerProvider(provider)
		
		TraceProvider = provider.(*sdk.TracerProvider)
		Tracer = provider.Tracer(serviceName)
		} else {
			// If tracing is disabled, use a Noop Tracer.
			TraceProvider = sdk.NewTracerProvider(
				sdk.WithSampler(sdk.NeverSample()),
			)

			TraceProvider.TracerProvider = noop.NewTracerProvider().TracerProvider

			otel.SetTracerProvider(TraceProvider)
			Tracer = TraceProvider.Tracer("nebula-noop")
		}
		
	if metricsEnabled {
		err := InitMetrics(ctx)
		if err != nil {
			logrus.Fatal(err)
		}
	} else {
		MeterProvider = metric.NewMeterProvider()
		MeterProvider.MeterProvider = moop.NewMeterProvider().MeterProvider
		Meter = MeterProvider.Meter("nebula")
	}

	if logsEnabled {
		err := InitLogger(ctx)
		if err != nil {
			logrus.Fatal(err)
	
		}
	}
}




// Flush ensures all OpenTelemetry data is exported before the application exits.
func Flush(ctx context.Context) {
	traceEnabled := viper.GetBool("otel.traces.enabled")
	metricsEnabled := viper.GetBool("otel.metrics.enabled")
	logsEnabled := viper.GetBool("otel.logs.enabled")

	if traceEnabled {
		if err := TraceProvider.Shutdown(ctx); err != nil {
			fmt.Printf("failed to shutdown tracer provider: %s", err.Error())
		}
	}

	if metricsEnabled {
		// Implement similar shutdown mechanisms for meter and logger providers if needed
		// This is just an example, adjust accordingly based on the actual SDK and implementation
		if err := MeterProvider.Shutdown(ctx); err != nil {
			fmt.Printf("failed to shutdown meter provider: %s", err.Error())
		}
	}

	if logsEnabled {
		if err := LoggerProvider.Shutdown(ctx); err != nil {
			fmt.Printf("failed to shutdown logger provider: %s", err.Error())
		}
	}

}