package otel

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func InitializeFlags(cmd *cobra.Command) error {

	// Set defaults
    viper.SetDefault("otel.traces.enabled", false)
    viper.SetDefault("otel.traces.exporter", "otlp") // Default tracing exporter
    viper.SetDefault("otel.metrics.enabled", false)
    viper.SetDefault("otel.metrics.exporter", "otlp") // Default metrics exporter
    viper.SetDefault("otel.logs.enabled", false)
    viper.SetDefault("otel.logs.exporter", "otlp")    // Default logs exporter
    viper.SetDefault("otel.service_name", "spakl-cli")
    viper.SetDefault("otel.endpoint", "http://localhost:4317")
    
	viper.SetDefault("otel.ignore", []string{})

    // Bind to environment variables
    viper.BindEnv("otel.traces.enabled", "OTEL_TRACES_ENABLED")
    viper.BindEnv("otel.traces.exporter", "OTEL_TRACES_EXPORTER")
    viper.BindEnv("otel.metrics.enabled", "OTEL_METRICS_ENABLED")
    viper.BindEnv("otel.metrics.exporter", "OTEL_METRICS_EXPORTER")
    viper.BindEnv("otel.logs.enabled", "OTEL_LOGS_ENABLED")
    viper.BindEnv("otel.logs.exporter", "OTEL_LOGS_EXPORTER")
    viper.BindEnv("otel.service_name", "OTEL_SERVICE_NAME")
    viper.BindEnv("otel.endpoint", "OTEL_EXPORTER_OTLP_ENDPOINT")
	viper.BindEnv("otel.ignore", "OTEL_MW_IGNORE")


	
	cmd.PersistentFlags().Bool("otel-traces", false, "Tracing enabled")
	err := viper.BindPFlag("otel.traces.enabled", cmd.PersistentFlags().Lookup("otel-traces"))
	if err != nil {
		return err
	}

	cmd.PersistentFlags().String("otel-traces-exporter", "otel", "Tracing exporter stdout or otel")
	err = viper.BindPFlag("otel.traces.exporter", cmd.PersistentFlags().Lookup("otel-traces-exporter"))
	if err != nil {
		return err
	}
	cmd.PersistentFlags().Bool("otel-metrics", false, "Otel Metrics enabled")
	err = viper.BindPFlag("otel.metrics.enabled", cmd.PersistentFlags().Lookup("otel-metrics"))
	if err != nil {
		return err
	}

	cmd.PersistentFlags().String("otel-metrics-exporter", "otel", "Tracing exporter stdout or otel")
	err = viper.BindPFlag("otel.metrics.exporter", cmd.PersistentFlags().Lookup("otel-metrics-exporter"))
	if err != nil {
		return err
	}

	cmd.PersistentFlags().Bool("otel-logs", false, "Otel Logs enabled")
	err = viper.BindPFlag("otel.logs.enabled", cmd.PersistentFlags().Lookup("otel-logs"))
	if err != nil {
		return err
	}

	cmd.PersistentFlags().String("otel-logs-exporter", "otel", "Tracing exporter stdout or otel")
	err = viper.BindPFlag("otel.logs.exporter", cmd.PersistentFlags().Lookup("otel-logs-exporter"))
	if err != nil {
		return err
	}

	cmd.PersistentFlags().String("otel-service-name", "spakl-cli", "Otel Service Name")
	err = viper.BindPFlag("otel.service_name", cmd.PersistentFlags().Lookup("otel-service-name"))
	if err != nil {
		return err
	}

	cmd.PersistentFlags().String("otel-endpoint", "http://localhost:4318", "Otel Collector Endpoint")
	err = viper.BindPFlag("otel.endpoint", cmd.PersistentFlags().Lookup("otel-endpoint"))
	if err != nil {
		return err
	}


	
	cmd.PersistentFlags().StringSlice("otel-ignore", []string{}, "Routes to ingore in otel propagation middleware")
	err = viper.BindPFlag("otel.ignore", cmd.PersistentFlags().Lookup("otel-ignore"))
	if err != nil {
		return err
	}


	return nil
}