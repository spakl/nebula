package otel

import (
	"context"
	"net/http"

	"github.com/spf13/viper"
	"go.opentelemetry.io/contrib/propagators/b3"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"
)

var (
	IgnoreRoutes []string = []string{}
)

func newPropagator() propagation.TextMapPropagator {
	b3Propagator := b3.New()
	return propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{},
		b3Propagator,
	)
}


func InjectTraceContext(ctx context.Context, req *http.Request) {
	otel.GetTextMapPropagator().Inject(ctx, propagation.HeaderCarrier(req.Header))
}


func ExtractTraceContext(req *http.Request) context.Context {
    return otel.GetTextMapPropagator().Extract(req.Context(), propagation.HeaderCarrier(req.Header))
}


func OtelPropagationMiddleware(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        // Extract context from incoming request
        ctx := ExtractTraceContext(r)

		ignorePaths := viper.GetStringSlice("otel.ignore")

		for _, path := range ignorePaths {
			if r.URL.Path == path {
				next.ServeHTTP(w, r) // if in ignore path, dont propagate
				return
			}
		}

		next.ServeHTTP(w, r.WithContext(ctx)) // Pass the new context down the chain
    })
}
