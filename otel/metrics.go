package otel

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/exporters/stdout/stdoutmetric"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/sdk/metric"
)

var (
	MeterProvider *metric.MeterProvider = metric.NewMeterProvider()
	Meter otelmetric.Meter = MeterProvider.Meter("nebula")
)

func InitMetrics(ctx context.Context) ( error) {
	otelEndpoint := viper.GetString("otel.endpoint")
	exporter := viper.GetString("otel.metrics.exporter")
	enabled := viper.GetBool("otel.metrics.enabled")
	
	if !enabled {
		return nil
	}


	res, err := getResource(ctx)
	if err != nil {
		panic(err)
	}

	var metricExporter metric.Exporter
	switch exporter {
	case "otel":
		metricExporter, err = newMetricExporter(ctx, otelEndpoint)
		if err != nil {
			logrus.Error(err)
			return err
		}
	case "stdout":
		metricExporter, err = newMetricConsoleExporter()
		if err != nil {
			logrus.Error(err)
			return err
		}
	default:
		metricExporter, err = newMetricConsoleExporter()
		if err != nil {
			logrus.Error(err)
			return err
		}
	}
	
	MeterProvider = metric.NewMeterProvider(
    metric.WithReader(
		metric.NewPeriodicReader(metricExporter, 
			metric.WithInterval(1*time.Minute),
		)),
		metric.WithResource(res),
	)
	
	Meter = MeterProvider.Meter("nebula")
	
	otel.SetMeterProvider(MeterProvider)
	
	return nil
	// is used, which fails to generate data.
}
func newMetricExporter(ctx context.Context, otelEndpoint string) (metric.Exporter, error) {
	return otlpmetrichttp.New(ctx,
		otlpmetrichttp.WithEndpointURL(otelEndpoint),
		otlpmetrichttp.WithTimeout(time.Second*10),
		otlpmetrichttp.WithInsecure(),
	)
}

func newMetricConsoleExporter() (metric.Exporter, error) {
	return stdoutmetric.New()
}

func NewCounter(name, desc, unit string) (otelmetric.Int64Counter, error) {
	return Meter.Int64Counter(
		name,
		otelmetric.WithDescription(desc),
		otelmetric.WithUnit(unit),
	)
}

func NewUpDownCounter(name, desc, unit string) (otelmetric.Int64UpDownCounter, error) {
	return Meter.Int64UpDownCounter(
		name,
		otelmetric.WithDescription(desc),
		otelmetric.WithUnit(unit),
	)
}

func NewHistogram(name, desc, unit string) (otelmetric.Float64Histogram, error) {
	return Meter.Float64Histogram(
		name,
		otelmetric.WithDescription(desc),
		otelmetric.WithUnit(unit),
	)
}

func NewGuage(name, desc, unit string) (otelmetric.Int64Gauge, error) {
	return Meter.Int64Gauge(
		name,
		otelmetric.WithDescription(desc),
		otelmetric.WithUnit(unit),
	)
}

func NewObsGuageInt(name string, desc string, unit string, callback func(_ context.Context, o otelmetric.Int64Observer) error) (otelmetric.Int64ObservableGauge, error) {
	return Meter.Int64ObservableGauge(
		name,
		otelmetric.WithDescription(desc),
		otelmetric.WithUnit(unit),
		otelmetric.WithInt64Callback(callback),
	)
}
func NewObsGuageFloat(name string, desc string, unit string, callback func(_ context.Context, o otelmetric.Float64Observer) error) (otelmetric.Float64ObservableGauge, error) {
	return Meter.Float64ObservableGauge(
		name,
		otelmetric.WithDescription(desc),
		otelmetric.WithUnit(unit),
		otelmetric.WithFloat64Callback(callback),
	)
}


