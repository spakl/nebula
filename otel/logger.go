package otel

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"go.opentelemetry.io/contrib/bridges/otellogrus"
	"go.opentelemetry.io/otel/exporters/otlp/otlplog/otlploghttp"
	"go.opentelemetry.io/otel/exporters/stdout/stdoutlog"
	"go.opentelemetry.io/otel/log/global"
	"go.opentelemetry.io/otel/sdk/log"
)


var LoggerProvider *log.LoggerProvider = log.NewLoggerProvider()

func InitLogger(ctx context.Context) error {
	otelEndpoint := viper.GetString("otel.endpoint")

	exporter := viper.GetString("otel.logs.exporter")
	
	res, err := getResource(ctx)
	if err != nil {
		panic(err)
	}

	var logExporter log.Exporter
	switch exporter {
	case "otel":
		logExporter, err = otlploghttp.New(ctx, 
			otlploghttp.WithEndpointURL(otelEndpoint + "/v1/logs"),
			otlploghttp.WithTimeout(time.Second*10),
			otlploghttp.WithInsecure(),
		)
		if err != nil {
			return err
		}
	case "stdout":
		logExporter, err = stdoutlog.New()
		if err != nil {
			return err
		}
	default:
		logExporter, err = stdoutlog.New()
		if err != nil {
			return err
		}
	}
	
	processor := log.NewBatchProcessor(logExporter)
	LoggerProvider = log.NewLoggerProvider(
		log.WithResource(res),
		log.WithProcessor(processor),
	)

	global.SetLoggerProvider(LoggerProvider) 

	hook := otellogrus.NewHook(
		"nebula", 
		otellogrus.WithLoggerProvider(LoggerProvider),
	)
	// Set the newly created hook as a global logrus hook
	logrus.AddHook(hook)
	
	return nil
}

