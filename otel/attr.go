package otel

import (
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/metric"
)

func WithMetricAttributes(attributes ...attribute.KeyValue) metric.MeasurementOption {
	return metric.WithAttributes(attributes...)
}

func NewStringAttr(name, value string) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.StringValue(value),
	}
}
func NewStringSliceAttr(name string, value []string) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.StringSliceValue(value),
	}
}

func NewBoolAttr(name string, value bool) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.BoolValue(value),
	}
}
func NewBoolSliceAttr(name string, value []bool) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.BoolSliceValue(value),
	}
}

func NewIntAttr(name string, value int) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.IntValue(value),
	}
}

func NewIntSliceAttr(name string, value []int) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.IntSliceValue(value),
	}
}

func NewInt64Attr(name string, value int64) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.Int64Value(value),
	}
}

func NewInt64SliceAttr(name string, value []int64) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.Int64SliceValue(value),
	}
}

func NewFloat64Attr(name string, value float64) attribute.KeyValue {
	return attribute.KeyValue{
		Key:   attribute.Key(name),
		Value: attribute.Float64Value(value),
	}
}
