package logger

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func InitFlags(cmd *cobra.Command){
	cmd.PersistentFlags().String("log-level", logrus.InfoLevel.String() , "Set the log level")
	viper.BindPFlag("log.level", cmd.PersistentFlags().Lookup("log-level"))

	cmd.PersistentFlags().String("log-format", "logfmt", "Specify the log format type: json or logfmt")
	viper.BindPFlag("log.format", cmd.PersistentFlags().Lookup("log-format"))
}