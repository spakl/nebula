package logger

import (
	"context"
	"strings"

	"go.opentelemetry.io/otel/attribute"
	"gitlab.com/spakl/nebula/v2/otel"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func InitLogger() {
	logFmt := viper.GetString("log.format")
	// logrus.SetOutput(os.Stdout)
	
	if logFmt == "json" {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	} else {
		logrus.SetFormatter(&logrus.TextFormatter{})
	}

	level := viper.GetString("log.level")
	pLvl, err := logrus.ParseLevel(level)
	if err != nil {
		logrus.Error(err.Error())
		logrus.SetLevel(logrus.InfoLevel)
	} else {
		logrus.SetLevel(pLvl)
	}

}

func GetFieldMap(ctx context.Context, fields []string) map[string]string {
	ctx, span := otel.CreateSpan(ctx, "GetFieldMap")
	defer span.End()

	logger := GetTraceLogger(ctx)

	// Parse fields into key-value pairs
	fieldMap := make(map[string]string)
	for _, field := range fields {
		pairs := strings.Split(field, ",")
		for _, pair := range pairs {
			kv := strings.SplitN(pair, "=", 2)
			if len(kv) == 2 {
				fieldMap[kv[0]] = kv[1]
				span.SetAttributes(attribute.KeyValue{Key: attribute.Key(kv[0]), Value: attribute.StringValue(kv[1]) })
			} else {
				logger.Warnf("Invalid key-value pair: %s", pair)
			}
		}
	}
	return fieldMap
}

func LogWFields(ctx context.Context, level string, fieldMap map[string]string, args []string){
	ctx, span := otel.CreateSpan(ctx, "LogWFields")
	defer span.End()


	var msg string
	var logger *logrus.Entry
	logger = GetTraceLogger(ctx)

	// Log the message with key-value pairs
	if len(args) == 0 {
		msg = "info called without a message"
	} else {
		msg = args[0]
	}
	
	for k, v := range fieldMap {
		logger = logger.WithField(k, v)
	}

	switch level {
	case "info":
		logger.Info(msg)
	case "error":
		logger.Error(msg)
	case "warn":
		logger.Warn(msg)
	case "debug":
		logger.Debug(msg)
	default:
		logger.Info(msg)
	}
	
}


func GetTraceLogger(ctx context.Context) *logrus.Entry {
	span := otel.GetSpanFromCtx(ctx)

	return logrus.WithContext(ctx).WithFields(logrus.Fields{
		"span_id": span.SpanContext().SpanID().String(),
		"trace_id": span.SpanContext().TraceID().String(),
	})
}