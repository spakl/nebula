package cfg

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/spakl/nebula/v2/utils"
)

// initConfig reads in config file and ENV variables if set.
func InitConfig(serviceName, cfgFile string) {
	if serviceName != "" {
		err := fmt.Errorf("unexpected service name: %s", serviceName)
		utils.PanicError(err)
	}

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cfgsvc" (without extension).
		viper.AddConfigPath(".")
		viper.AddConfigPath(fmt.Sprintf("/%s", serviceName))
		viper.AddConfigPath(fmt.Sprintf("/etc/%s", serviceName))
		viper.AddConfigPath(home)
		viper.SetConfigType("yml")
		viper.SetConfigName(fmt.Sprintf(".%s", serviceName))
	}

	viper.SetEnvPrefix(serviceName)
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}
