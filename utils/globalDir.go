package utils

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
)

func GetGlobalDir() (string, error) {
	// will use flag value and default value
	path := viper.GetString("globalDir")

	home, err := os.UserHomeDir()
	if err != nil {
		return "", err
	}

	switch path {
	case "HOME":
		path = fmt.Sprintf("%s/spakl", home)
		if _, err := os.Stat(path); os.IsNotExist(err) {
			return "", err
		}
		return path, nil
	case "":
		path = fmt.Sprintf("%s/spakl", home)
		if _, err := os.Stat(path); os.IsNotExist(err) {
			return "", err
		}
		return path, nil
	default:
		if _, err := os.Stat(path); os.IsNotExist(err) {
			return "", err
		}
		return path, nil
	}
}
