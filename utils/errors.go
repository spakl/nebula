package utils

import (
	"log"
)

// Helper function to handle the panic and log the error
func PanicError(err error) {
	if err != nil {
		log.Fatalf("panic occurred: %v", err) // Optional: Log and panic
		panic(err)                            // Trigger panic
	}
}