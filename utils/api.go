package utils

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/spakl/nebula/v2/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/metric"
)



func SendError(ctx context.Context,r *http.Request, w http.ResponseWriter, err error, status int) error {
	ctx, span := otel.CreateSpan(r.Context(), "api.SendError")
	defer span.End()
	otel.AddErrorEvent(span, err)
	errorCodeText := http.StatusText(status)

	errorMetric, _ :=	otel.NewCounter("nebula.utils.ErrorsSent", "Errors Sent Using utility", "count")
	errorMetric.Add(ctx, 1, metric.WithAttributes(
		attribute.String("method", r.Method),
		attribute.Int("status", status),
		attribute.String("codeDescription", errorCodeText),
		attribute.String("path", r.URL.String()),
	))

	resp := map[string]any{
		"path": r.URL.String(),
		"method": r.Method,
		"status": "failed",
		"error": err.Error(),
		"code": status,
		"codeDescription": errorCodeText,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(resp)
}

func SendResponse(ctx context.Context,r *http.Request, w http.ResponseWriter, data any, status int) error {
	ctx, span := otel.CreateSpan(r.Context(), "api.SendResponse")
	defer span.End()


	codeText := http.StatusText(status)
	resp := map[string]any{
		"path": r.URL.String(),
		"method": r.Method,
		"status": "success",
		"data": data,
		"code": status,
		"codeDescription": codeText,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(resp)
}


