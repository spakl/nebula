package utils

import (
	"crypto/tls"
	"crypto/x509"
	"os"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func ConfigureTls() *tls.Config {

	caCertFiles := viper.GetStringSlice("tls.caCertFiles")
	certFile := viper.GetString("tls.certFile")
	keyFile := viper.GetString("tls.keyFile")
	insecureSkipVerify := viper.GetBool("tls.insecureSkipVerify")
	logrus.Debugf("certFile %s", certFile)
	logrus.Debugf("keyFile %s", keyFile)

	caCertPool := x509.NewCertPool()
	// Load client cert
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		logrus.Warn(err)
	}
	// https://gist.github.com/michaljemala/d6f4e01c4834bf47a9c4
	// Load CA cert
	if len(caCertFiles) > 0 {
		for _, file := range caCertFiles {
			configCaCert, err := os.ReadFile(file)
			if err != nil {
				logrus.Warn(err)
			}
			caCertPool.AppendCertsFromPEM(configCaCert)
		}
	}
	// Setup HTTPS client
	return &tls.Config{
		Certificates:       []tls.Certificate{cert},
		RootCAs:            caCertPool,
		ClientCAs:          caCertPool,
		InsecureSkipVerify: insecureSkipVerify,
	}
}
